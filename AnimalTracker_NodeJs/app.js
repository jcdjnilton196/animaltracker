const SerialPort = require('serialport')
const Readline = require('@serialport/parser-readline')
const port = new SerialPort("COM3", { baudRate: 9600 })

// Firebase App (the core Firebase SDK) is always required and
// must be listed before other Firebase SDKs
var firebase = require("firebase/app");

// Add the Firebase products that you want to use
require("firebase/auth");
require("firebase/firestore");


// TODO: Replace the following with your app's Firebase project configuration
const firebaseConfig = {
    apiKey: "AIzaSyAx_V8nHxFmezs86xe4evizVrj9L-VltTo",
    authDomain: "animaltracker-ceub.firebaseapp.com",
    databaseURL: "https://animaltracker-ceub.firebaseio.com",
    projectId: "animaltracker-ceub",
    storageBucket: "animaltracker-ceub.appspot.com",
    messagingSenderId: "226663065724",
    appId: "1:226663065724:web:7b6ca631e6ad20a57dc1f9",
    measurementId: "G-JQ472EF5Y4"
  };
  
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

  var admin = require("firebase-admin");

var serviceAccount = require("./animaltracker-ceub-firebase-adminsdk-t0xtf-d756cbcbca.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://animaltracker-ceub.firebaseio.com"
});

let db = admin.firestore();

const parser = new Readline()
port.pipe(parser)

parser.on('data', function(line) {
    firebase.auth()
    console.log("O GPS do Animal Tracker aguarda pelo sinal dos satelites...");
    line = line.replace(/(\r\n|\n|\r)/gm,"")
    //console.log(line)

    try{
        const gpsData = JSON.parse(line);
        console.log(gpsData)
        console.log(gpsData.latitude)
        console.log(gpsData.longitude)
        console.log(gpsData.hora);
        console.log(gpsData.data);

        let docRef = db.collection('localizacao').doc();
            docRef.set({
                latitude: gpsData.latitude,
                longitude: gpsData.longitude,
                hora: gpsData.hora,
                data: gpsData.data
            });
    }

    catch(error){

    }
})
//> ROBOT ONLINE