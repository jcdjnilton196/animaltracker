#include <Ethernet.h>

#include <SoftwareSerial.h>
#include <TinyGPS.h>

SoftwareSerial serial1(10, 11); // RX, TX
TinyGPS gps1;

void setup() {
   serial1.begin(9600);
   Serial.begin(9600);
}

void loop() {
  bool recebido = false;

  while (serial1.available()) {
     char cIn = serial1.read();
     recebido = gps1.encode(cIn);
  }

  if (recebido) {

    String jsonData = "{";
    
     long latitude, longitude;
     unsigned long idadeInfo;
     gps1.get_position(&latitude, &longitude, &idadeInfo);     

     //Recebendo os dados de Latitude
     if (latitude != TinyGPS::GPS_INVALID_F_ANGLE) {
      String valor = String(float(latitude) / 100000, 6);
        jsonData = jsonData + "\"latitude\":" + valor + ",";
     }
     
     //Recebendo os dados de Longitude
     if (longitude != TinyGPS::GPS_INVALID_F_ANGLE) {
      String valor = String(float(longitude) / 100000, 6);
        jsonData= jsonData + "\"longitude\":" + valor + ",";
     }


     //Dia e Hora
     int ano;
     byte mes, dia, hora, minuto, segundo, centesimo;
     gps1.crack_datetime(&ano, &mes, &dia, &hora, &minuto, &segundo, &centesimo, &idadeInfo);

     jsonData = jsonData + "\"data\":\""+dia+"/"+mes+"/"+ano+"\",";
       
     jsonData = jsonData + "\"hora\":\""+hora+":"+minuto+":"+segundo+":"+centesimo+"\"}";
     
     //Printando os dados na API
     Serial.println(jsonData);
     delay(5000);

  }
}
